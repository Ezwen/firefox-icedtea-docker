# Firefox + Icedtea + OpenVPN avec Docker

## Description

Pour lancer un vieux Firefox pouvant faire tourner des applets Java avec IcedTea, et pouvant accéder à openvpn. Fonctionne avec Docker et X11.

L'image actuelle repose sur :
- Ubuntu 16.04
- Firefox 51.0b14
- openjdk 8
- icedtea 8
- openvpn 2.3.2

## Utilisation

```
$ git clone https://framagit.org/Gwendal/firefox-icedtea-docker.git
$ cd firefox-icedtea-docker
$ mkdir profile-firefox-icedtea
$ sudo docker-compose down && sudo docker-compose up
```
Le premier lancement prendra assez longtemps car l'image docker devra être construite.

## Mise à jour

Si besoin de reconstruire l'image parce que le `Dockerfile` aurait changé :

```
$ cd firefox-icedtea-docker
$ git pull
$ sudo docker-compose build
$ sudo docker-compose down && sudo docker-compose up
```
