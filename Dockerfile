FROM ubuntu:16.04

WORKDIR /firefox

# Install required packages
RUN apt-get update \
    && apt-get install -y default-jre icedtea-plugin wget tar bzip2 gtk+3.0 openvpn

# Install firefox
RUN  wget https://ftp.mozilla.org/pub/firefox/releases/51.0b14/firefox-51.0b14.linux-x86_64.sdk.tar.bz2 \
    && tar -C /firefox -xf /firefox/firefox-51.0b14.linux-x86_64.sdk.tar.bz2 

# Adding a proper 'developer' user, necessary to run GUI apps
# If necessary, replace 1000 with your user / group id
RUN export uid=1000 gid=1000 && \
    mkdir -p /home/developer && \
    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    chown ${uid}:${gid} -R /home/developer

# Install sudo and make 'developer' a passwordless sudoer
RUN apt-get install sudo
ADD ./developersudo /etc/sudoers.d/developersudo

# Replacing pkexec by sudo
RUN rm /usr/bin/pkexec
RUN ln -s /usr/bin/sudo /usr/bin/pkexec

# Disable chrooting in openvpn, to avoid some bugs
RUN mv /var/lib/openvpn/chroot /var/lib/openvpn/chroot.disabled

USER developer

ENV HOME /home/developer

CMD /firefox/firefox-sdk/bin/firefox-bin -new-instance
